pragma solidity ^0.5.0;

import './TRC20.sol';

contract token4XTron is TRC20 {
    address public owner;

    event Minted(address indexed account, uint256 amount);
    event Burned(address indexed account, uint256 amount);
    
    modifier onlyOwner() {
        require(msg.sender == owner,"Permission Denied: Only owner can access this resource");
        _;
    }
    
    constructor() TRC20("4XTRON", "4XT", 6) public {
        owner = msg.sender;
        
        _mint(owner, 50000000000000);
    }

    function mint(address account, uint256 amount) onlyOwner public {
        require(amount > 0, "TRC20: amount should be valid");
        _mint(account, amount);

        emit Minted(account, amount);
    } 

    function burn(address account, uint256 amount) onlyOwner public {
        require(amount > 0, "TRC20: amount should be valid");
        _burn(account, amount);

        emit Burned(account, amount);
    } 
}